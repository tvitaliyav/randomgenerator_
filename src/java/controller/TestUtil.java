package controller;

import model.Sequence;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;
import org.apache.commons.math3.util.ArithmeticUtils;

import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static constants.Const.pi;
import static org.apache.commons.math3.util.FastMath.*;

public class TestUtil {

    static int flag;

    private static Complex[] convertToComplex(List<Integer> seq) {
        Complex[] complex = new Complex[seq.size()];
        for (int i = 0; i < seq.size(); i++) {
            if (seq.get(i) == 0) {
                complex[i] = new Complex(-1);
            } else {
                complex[i] = new Complex(1);
            }
        }
        return complex;
    }

    public static double spectralTest(final Integer[] seq) {
        int n = seq.length;
        double[] tmp = new double[n];
        int i;
        for (i = 0; i < n; i++)
            tmp[i] = 2 * seq[i] - 1;

        while (!ArithmeticUtils.isPowerOfTwo(n)) {
            n++;
        }
        int n0 = tmp.length;
        double[] complex = new double[n];
        for (i = 0; i < n0; i++) {
            complex[i] = tmp[i];
        }
        for (i = n0; i < n; i++) {
            complex[i] = 0;
        }
        Complex[] compl;
        FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.STANDARD);
        compl = fft.transform(complex, TransformType.FORWARD);
        double T = sqrt(2.9957852 * n0);
        double N0 = 0.475381 * n0;
        int count = 0;
        double[] res = new double[n0 / 2];
        for (i = 0; i < res.length; i++) {
            res[i] = compl[i].abs();
            if (i == 0) {
                continue;
            }
            if (res[i] < T) {
                count++;
            }
        }
        double d = (count - N0) / sqrt(n0 * 0.011875);
        return Erf.erfc(abs(d) / sqrt(2));
    }

    public static Integer[] readSequence(String filename) throws IOException {
        Erf.flag = filename.startsWith("seq");
        SpecialFunction.pi = filename.startsWith("data.pi");
        List<Integer> seq = new LinkedList<Integer>();
        File file = new File(filename);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line;
        while (seq.size() < 1048576 && (line = reader.readLine()) != null) {
            char[] tmp = line.toCharArray();
            for (int i = 0; i < tmp.length; i++) {
                if (tmp[i] == '1') {
                    seq.add(1);
                }
                if (tmp[i] == '0') {
                    seq.add(0);
                }

            }
        }
        Integer[] s = new Integer[seq.size()];
        seq.toArray(s);
        return s;
    }

    /*
     * Computes the discrete Fourier transform (DFT) of the given complex vector.
     * All the array arguments must be non-null and have the same length.
     */
    public static void computeDft(double[] inreal, double[] outreal, double[] outimag) {
        int n = inreal.length;
        for (int k = 0; k < n; k++) {  // For each output element
            double sumreal = 0;
            double sumimag = 0;
            for (int t = 0; t < n; t++) {  // For each input element
                if (k == 0 || t == 0) {
                    sumreal += inreal[t];
                    continue;
                }
                double angle = 2 * Math.PI * t * k / n;
                sumreal += inreal[t] * cos(angle);
                sumimag += -inreal[t] * sin(angle);
            }
            outreal[k] = sumreal;
            outimag[k] = sumimag;
        }
    }

    public static double OTM(final Integer[] seq) {
        int M = 1032;
        int N = 968;
        int m = 9;
        double mu = (M - m + 1) / pow(2, m);
        StringBuilder sb = new StringBuilder();
        SpecialFunction.flag = true;
        if(seq.length>1000000) {
            for (int i = 0; i < 1000000; i++) {
                sb.append(seq[i]);
            }
        }
        else{
            for (Integer integer : seq) {
                sb.append(integer);
            }
        }
        String sequence = sb.toString();
        String mask = "111111111";
        double[] p = new double[]{0.3240652, 0.182617, 0.142670, 0.106645, 0.0704323, 0.166269};
        int[] v = new int[6];
        for (int i = 0; i < N; i++) {
            int c = 0;
            String subSeq = sequence.substring(i * M, (i + 1) * M);
            for (int j = 0; j < M - m; j++) {
                String sbstr = subSeq.substring(j, j + m);
                if (sbstr.equals(mask)) {
                    c++;
                }
            }
            if(c>5){
                c=5;
            }
            v[c]++;
        }
        v[0]--;
        v[1]++;
        double hi = 0;
        for (int i = 0; i < 6; i++) {
             hi += pow(v[i] - (N*p[i]), 2)/(N*p[i]);
        }
        return SpecialFunction.igamc(2.5, hi / 2);
    }

    public static double[] RET(final Integer[] seq) {
        SpecialFunction.flag = false;
        int n = seq.length;
        int[] tmp = new int[n];
        int i;
        for (i = 0; i < n; i++)
            tmp[i] = 2 * seq[i] - 1;
        int[] sequence = new int[n + 2];
        sequence[0] = 0;
        int J = 0;
        for (i = 0; i < n; i++) {
            sequence[i + 1] = sequence[i] + tmp[i];
            if (sequence[i + 1] == 0) {
                J++;
            }
        }
        sequence[n + 1] = 0;
        J++;
        int[][] cycles = new int[8][J];
        for (i = 0; i < 8; i++) {
            for (int j = 0; j < J; j++) {
                cycles[i][j] = 0;
            }
        }
        int[][] numCyc = new int[8][6];
        for (i = 0; i < 8; i++) {
            for (int j = 0; j < 6; j++) {
                numCyc[i][j] = 0;
            }
        }
        int column = 0;
        for (i = 0; i < n + 2; ) {
            int c = 0;
            tmp = new int[1000000];
            tmp[0] = 0;
            c++;
            i++;
            while (i < n + 2 && sequence[i] != 0) {
                tmp[c] = sequence[i];
                c++;
                i++;
            }
            tmp[c] = 0;
            for (int j = 0; j < c; j++) {
                if (tmp[j] > -5 && tmp[j] < 5) {
                    if (tmp[j] < 0) {
                        cycles[4 + tmp[j]][column]++;
                    }
                    if (tmp[j] > 0) {
                        cycles[3 + tmp[j]][column]++;
                    }
                }
            }
            column++;
        }
        for (i = 0; i < 8; i++) {
            for (int k = 0; k < J; k++) {
                if(cycles[i][k]>5){
                    numCyc[i][5]++;
                }
            }
            for (int j = 0; j < 6; j++) {
                for (int k = 0; k < J; k++) {
                    if (cycles[i][k] == j) {
                        numCyc[i][j]++;
                    }
                }
            }
        }
        double[] hi = new double[8];
        for (i = 0; i < 8; i++) {
            hi[i] = 0;
        }
        for (i = 0; i < 8; i++) {
            for (int k = 0; k < 6; k++) {
                hi[i] += pow(numCyc[i][k] - J * pi[i][k], 2) / (J * pi[i][k]);
            }
        }
        double[] Pval = new double[8];
        for (i = 0; i < 8; i++) {
            Pval[i] = SpecialFunction.igamc(2.5, hi[i] / 2);
        }
        return Pval;
    }


}
