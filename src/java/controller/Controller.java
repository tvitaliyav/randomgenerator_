package controller;

import model.Sequence;

import java.io.IOException;
import java.util.Scanner;

import static constants.Const.B1;

public class Controller {

    Sequence sequence;

    public void run() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину последовательности");
        int length = scanner.nextInt();
        if (length % 4 == 0) {
            length /= 4;
        }
        else{
            length/=4;
            length++;
        }
        System.out.println("Выберите режим работы:\n " +
                "1. Генерация со стандартным В\n " +
                "2. Генерация с пользовательским В");
        int key = scanner.nextInt();
        switch (key) {
            case 1:
                sequence = Util.run(length, B1);
                break;
            case 2:
                sequence = Util.run(length, Util.createB());
                break;
            default:
                System.out.println("Вы ввели не то число, перезапускайте программу О_О");
        }
        System.out.println("Посчитать период?\n " +
                "1. Да\n " +
                "2. Нет");
        key = scanner.nextInt();
        switch (key) {
            case 1:
                System.out.println(Util.period(sequence));
                break;
            case 2:
                break;
            default:
                System.out.println("Вы ввели не то число, перезапускайте программу О_О");
        }
    }

}
