import java.util.Arrays;

public class apl {

    public static void main(String[] args) {
        int M = 20;
        int N = 3;
        int m =9;
        String sequence = "011111111100110011101100111100000011111000000011111111111110";
        String mask = "111111111";
        int[] v = new int[6];
        for (int i = 0; i < N; i++) {
            int c = 0;
            String subSeq = sequence.substring(i * M, (i + 1) * M);
            for (int j = 0; j < M - m; j++) {
                String sbstr = subSeq.substring(j, j + m);
                if (sbstr.equals(mask)) {
                    c++;
                }
            }
            if(c>5){
                c=5;
            }
            v[c]++;

        }
        System.out.println(Arrays.toString(v));
    }
}
