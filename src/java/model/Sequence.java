package model;

import javafx.beans.binding.StringBinding;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Sequence {
    private List<Integer> seq;

    public Sequence() {
        seq = new ArrayList();
    }

    public List<Integer> getSeq() {
        return seq;
    }

    public void setSeq(List<Integer> seq) {
        this.seq = seq;
    }

    public void add(int n) {
        seq.add(n);
    }

    public int get(int n){
        return seq.get(n);
    }

    public int size(){
        return seq.size();
    }

    private char[] toBin(int n){
        String bin = Integer.toBinaryString(n);
        char[] b = bin.toCharArray();
        if(b.length<4){
            if(b.length ==1) {
                char[] tmp = {0, 0, 0, b[0]};
               return tmp;
            }
            if(b.length ==2) {
                char[] tmp = {0, 0, b[0], b[1]};
                return tmp;
            }
            if(b.length ==3) {
                char[] tmp = {0, b[0], b[1], b[2]};
                return tmp;
            }
        }
        return b;
    }

    public void calculate(int x1, int x2) {
        char[] b = toBin(x1^x2);
        for(int i = 0; i<b.length; i++){
            if(b[i] == '1'){
                add(1);
            }
            else{
                add(0);
            }
        }
//        System.out.println(x1^x2);
    }

    public void print() throws IOException {
        File file = new File("sequence.txt");
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter out = new BufferedWriter(fileWriter);
        for (int i = 0; i < seq.size(); i++) {
            out.write(seq.get(i) + " ");
        }
        System.out.println(seq);
        out.write("\n");
        out.close();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i =0; i<seq.size(); i++){
            stringBuilder.append(seq.get(i));
        }
        return stringBuilder.toString();
    }
}
