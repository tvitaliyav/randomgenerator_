import controller.Controller;
import controller.TestUtil;
import controller.Util;
import model.Sequence;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class App {
    public static void main(String[] args) throws IOException {

        new Controller().run();

        System.out.println(TestUtil.spectralTest(TestUtil.readSequence("data.e")));
        System.out.println(TestUtil.spectralTest(TestUtil.readSequence("data.pi")));
        System.out.println(TestUtil.spectralTest(TestUtil.readSequence("sequence.txt")));

        System.out.println(TestUtil.OTM(TestUtil.readSequence("data.e")));
        System.out.println(TestUtil.OTM(TestUtil.readSequence("data.pi")));
        System.out.println(TestUtil.OTM(TestUtil.readSequence("sequence.txt")));

        System.out.println(TestUtil.RET(TestUtil.readSequence("data.e"))[4]);
        System.out.println(TestUtil.RET(TestUtil.readSequence("data.pi"))[4]);
        System.out.println(TestUtil.RET(TestUtil.readSequence("sequence.txt"))[4]);

    }
}
